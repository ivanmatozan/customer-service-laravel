@extends('layouts.app')

@section('content')
    <div class="row">

        <h3 class="page-header">Edit Response</h3>

        <form  class="form-group" action="{{ url('response/'.$response->id.'/update') }}" method="post">

            {!! csrf_field() !!}
            {{ method_field('PATCH') }}

            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                <label for="body" class="control-label">Reply</label>
                <textarea class="form-control vresize" id="body" name="body" rows="4">{{ $response->body }}</textarea>
                @if ($errors->has('body'))
                    <span class="help-block">
                        <strong>{{ $errors->first('body') }}</strong>
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-default">Update</button>
            <a href="/question/{{$response->getQuestion()->id}}" class="btn btn-info">Cancel</a>
        </form>
    </div>
@endsection


