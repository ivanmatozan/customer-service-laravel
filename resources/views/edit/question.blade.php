@extends('layouts.app')

@section('content')
    <!-- Form for new questions -->
    <div class="row">

        <h3 class="page-header">{{ $question->title }}</h3>

        <form  class="form-group" action="{{ url('question/'.$question->id.'/update') }}" method="post">

            {!! csrf_field() !!}
            {{ method_field('PATCH') }}

            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title" class="control-label">Title</label>
                <input type="text" class="form-control" id="title" name="title" value="{{ $errors->isEmpty() ? $question->title : old('title') }}">
                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                <label for="body" class="control-label">Question</label>
                <textarea class="form-control vresize" id="body" name="body" rows="4">{{ $errors->isEmpty() ? $question->body : old('body')  }}</textarea>
                @if ($errors->has('body'))
                    <span class="help-block">
                        <strong>{{ $errors->first('body') }}</strong>
                    </span>
                @endif
            </div>

            <button type="submit" class="btn btn-default">Update</button>
            <a href="{{'/'}}" class="btn btn-info">Cancel</a>
        </form>
    </div>
@endsection


