@extends('layouts.app')

@section('content')
    <!-- Display question -->
    <div class="row row-centered">

        <h3 class="page-header">{{ $question->title }}</h3>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <span>Title: {{ $question->title }}</span>
                <span class="pull-right">Author: {{ $question->user()->first()->name }}</span>
            </div>
            <div class="panel-body">
                {{ $question->body }}
                <br><br>

                @if($question->isActive() && Auth::user()->isAdmin())
                    <a href="{{ url('question/'.$question->id.'/close') }}" class="btn btn-success">Close</a>
                @elseif(!$question->isActive() && Auth::user()->isAdmin())
                    <a href="{{ url('question/'.$question->id.'/open') }}" class="btn btn-warning">Open</a>
                @endif

                @if(!Auth::user()->isAdmin() && $question->isActive() && !$question->hasResponse())
                    <a href="{{ url('question/'.$question->id.'/edit') }}" class="btn btn-warning pull">Edit</a>
                @endif

                <a href="{{ url('question/'.$question->id.'/delete') }}" class="btn btn-danger">Delete</a>
            </div>
        </div>

        <!-- Display responses -->
        @if(count($responses) > 0)
            @foreach($responses as $response)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Reply</span>
                        <span class="pull-right">Author: {{ $response->getUser()->name }}</span>
                    </div>
                    <div class="panel-body">
                        {{ $response->body }}
                        @if($question->lastResponse()->id == $response->id && $response->getUser()->id == Auth::user()->id && $question->isActive())
                            <br><br>
                            <a href="{{ url('response/'.$response->id.'/edit') }}" class="btn btn-warning">Edit</a>
                            <a href="{{ url('response/'.$response->id.'/delete') }}" class="btn btn-danger">Delete</a>
                        @endif
                    </div>
                </div>
            @endforeach
        @else
            <div class="panel panel-danger">
                <div class="panel-heading">No Replies</div>
            </div>
        @endif

        @if($question->isActive())
            <!-- New response form -->
            <form  class="form-group" action="{{ url('response/'.$question->id.'/store') }}" method="post">

                {!! csrf_field() !!}

                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                    <label for="body" class="control-label">Reply</label>
                    <textarea class="form-control vresize" id="body" name="body" rows="4"></textarea>
                    @if ($errors->has('body'))
                        <span class="help-block">
                        <strong>{{ $errors->first('body') }}</strong>
                    </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-default">Send</button>
                    <a href="{{ '/' }}" class="btn btn-info pull-right">Back</a>
            </form>
        @else
            <a href="{{ '/' }}" class="btn btn-info pull-right">Back</a>
        @endif
    </div>
@endsection


