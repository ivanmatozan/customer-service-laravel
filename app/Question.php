<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body',
    ];

    // Question has many reponses
    public function responses() {
        return $this->hasMany(Response::class);
    }

    // Question belongs to user
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Check if question still active
     *
     * @return bool
     */
    public function isActive() {
        return $this->active;
    }

    /**
     * Check if question has response
     *
     * @return bool
     */
    public function hasResponse() {
        if ($this->responses()->count() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get last response in question
     *
     * @return Response
     */
    public function lastResponse() {
        return $this->responses()->orderBy('created_at', 'desc')->first();
    }
}
