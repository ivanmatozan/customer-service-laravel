<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'is_admin', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Check if user is admin
     *
     * @return bool
     */
    public function isAdmin() {
        return $this->is_admin;
    }

    // User has many questions
    public function questions() {
        return $this->hasMany(Question::class);
    }

    // User has many responses
    public function responses() {
        return $this->hasMany(Response::class);
    }
}
