<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body',
    ];
    
    // Response belongs to user
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    // Response belongs to question
    public function question() {
        return $this->belongsTo(Question::class);
    }

    /**
     * Get owner of the response
     *
     * @return User
     */
    public function getUser() {
        return $this->user()->first();
    }

    /**
     * Get owner of the response
     *
     * @return User
     */
    public function getQuestion() {
        return $this->question()->first();
    }
}
