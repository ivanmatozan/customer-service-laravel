<?php

namespace App\Http\Controllers;

use App\Response;
use Gate;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Requests;

class ResponseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    // Store new question
    public function store(Request $request, Question $question) {
        $this->validate($request, [
            'body' => 'required',
        ]);

        $response= new Response;

        $response->user_id = $request->user()->id;
        $response->question_id = $question->id;
        $response->body = $request->body;

        $response->save();

        return back();
    }

    // Delete response
    public function delete(Response $response) {
        $questionLastResponse = $response->getQuestion()->responses()->orderBy('created_at', 'desc')->first();

        if (Gate::denies('delete-response', $response) || $questionLastResponse->id != $response->id) {
            return redirect('question/'.$response->getQuestion()->id);
        }

        $response->delete();

        return back();
    }

    // Edit response
    public function edit(Response $response) {
        $questionLastResponse = $response->getQuestion()->responses()->orderBy('created_at', 'desc')->first();

        if (Gate::denies('edit-response', $response) || $questionLastResponse->id != $response->id) {
            return redirect('question/'.$response->getQuestion()->id);
        }

        return view('edit.response', [
            'response' => $response,
        ]);
    }

    // Update question
    public function update(Request $request, Response $response) {
        $this->validate($request, [
            'body' => 'required',
        ]);

        $response->update($request->all());

        return redirect('question/'.$response->getQuestion()->id);
    }
}
