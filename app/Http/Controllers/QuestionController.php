<?php

namespace App\Http\Controllers;

use Illuminate\Support\ServiceProvider;
use Gate;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Requests;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    // Store new question
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required|max:255',
            'body' => 'required',
        ]);
        
        $question = new Question;

        $question->user_id = $request->user()->id;
        $question->title = $request->title;
        $question->body = $request->body;

        $question->save();

        return back();
    }

    // Display question
    public function display(Question $question) {
        $question->get();

        $responses = $question->responses()->orderBy('created_at', 'asc')->get();

        return view('pages.question', [
            'question' => $question,
            'responses' => $responses
        ]);
    }

    // Delete question
    public function delete(Question $question) {
        if (Gate::denies('delete-question', $question)) {
            return redirect('/');
        }
        
        $question->responses()->delete();

        $question->delete();

        return redirect('/');
    }

    // Close question
    public function close(Question $question) {
        $question->active = false;

        $question->save();

        return back();
    }

    // Open question
    public function open(Question $question) {
        $question->active = true;

        $question->save();

        return back();
    }

    // Edit question
    public function edit(Question $question) {
        if (Gate::denies('edit-question', $question)) {
            return redirect('/');
        }

        return view('edit.question', [
            'question' => $question,
        ]);
    }

    // Update question
    public function update(Request $request, Question $question) {
        $this->validate($request, [
            'title' => 'required|max:255',
            'body' => 'required',
        ]);
        
        $question->update($request->all());

        return redirect('question/'.$question->id);
    }
}
