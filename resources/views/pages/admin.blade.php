@extends('layouts.app')

@section('content')

    <!-- List of questions -->
    <div class="row">
        <h3 class="page-header">Question List</h3>

        <ul class="list-group">
            @if(count($questions) > 0)
                @foreach($questions as $question)
                    @if($question->isActive())
                        @if($question->hasResponse() && $question->lastResponse()->getUser()->isAdmin())
                            <div class="panel panel-danger">
                        @else
                            <div class="panel panel-warning">
                        @endif
                    @else
                        <div class="panel panel-success">
                    @endif
                        <div class="panel-heading">
                            <span>Title: {{ $question->title }}</span>
                            <span class="pull-right">Author: {{ $question->user()->first()->name }}</span>
                        </div>
                        <div class="panel-body">
                            {{ $question->body }}
                        </div>
                        <div class="panel-footer">
                            <a href="{{ url('question/'.$question->id) }}" class="btn btn-info">View</a>
                            @if($question->isActive())
                                <a href="{{ url('question/'.$question->id.'/close') }}" class="btn btn-success">Close</a>
                            @else
                                <a href="{{ url('question/'.$question->id.'/open') }}" class="btn btn-warning">Open</a>
                            @endif
                            <a href="{{ url('question/'.$question->id.'/delete') }}" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="panel panel-success">
                    <div class="panel-heading">No Questions</div>
                </div>
            @endif
        </ul>
    </div>
@endsection