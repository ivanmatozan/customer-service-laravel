<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('delete-question', function ($user, $question) {
            if ($user->isAdmin()) {
                return true;
            }
            return $user->id == $question->user_id;
        });

        $gate->define('delete-response', function ($user, $response) {
            return $user->id == $response->user_id;
        });

        $gate->define('edit-question', function ($user, $question) {
            if ($user->isAdmin()) {
                return false;
            }
            return $user->id == $question->user_id;
        });

        $gate->define('edit-response', function ($user, $response) {
            return $user->id == $response->user_id;
        });
    }
}
