@extends('layouts.app')

@section('content')
    <!-- Form for new questions -->
    <div class="row col-md-5">

        <h3 class="page-header">New Question</h3>

        <form  class="form-group" action="{{ url('question/store') }}" method="post">

            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title" class="control-label">Title</label>
                <input type="text" class="form-control" id="title" name="title"  value="{{ old('title') }}">
                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                <label for="body" class="control-label">Question</label>
                <textarea class="form-control vresize" id="body" name="body" rows="4">{{ old('body') }}</textarea>
                @if ($errors->has('body'))
                    <span class="help-block">
                        <strong>{{ $errors->first('body') }}</strong>
                    </span>
                @endif
            </div>

            <button type="submit" class="btn btn-default">Send</button>
        </form>
    </div>

    <!-- List of questions -->
    <div class="row col-md-6 col-md-offset-1">
        <h3 class="page-header">Question List</h3>

        <ul class="list-group">
            @if(count($questions) > 0)
                @foreach($questions as $question)
                    @if($question->isActive())
                        @if($question->hasResponse() && $question->lastResponse()->getUser()->isAdmin())
                            <div class="panel panel-warning">
                        @else
                            <div class="panel panel-danger">
                        @endif
                    @else
                        <div class="panel panel-success">
                    @endif
                        <div class="panel-heading">
                            <span>Title: {{ $question->title }}</span>
                            <span class="pull-right">Author: {{ $question->user()->first()->name }}</span>
                        </div>
                        <div class="panel-body">
                            {{ $question->body }}
                        </div>
                        <div class="panel-footer">
                            <a href="{{ url('question/'.$question->id) }}" class="btn btn-info">View</a>
                            @if($question->isActive() && !$question->hasResponse())
                                <a href="{{ url('question/'.$question->id.'/edit') }}" class="btn btn-warning">Edit</a>
                            @endif
                            <a href="{{ url('question/'.$question->id.'/delete') }}" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="panel panel-success">
                    <div class="panel-heading">No Questions</div>
                </div>
            @endif
        </ul>
    </div>
@endsection


