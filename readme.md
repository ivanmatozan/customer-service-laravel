# Customer Service - zadatak

Koristiš laravel + boostrap za frontend, jquery po potrebi.

Znači zadatak ti je složiti najjednostavniji oblik web službe za korisnike.

Registracija i login.

Dvije user role: user i admin.

User postavlja pitanja, admin na njih odgovara.

Na jedno pitanje može biti više komentara, u smisli kad se useru  javi admin da se oni mogu dopisivati.

Admin može označiti koje je pitanje riješeno.

User vidi svoje upite.

Admin sve, gdje je naznačeno koji su riješeni a koji ne.

Editiranje i brisanje svega, razmisli malo tko bi imao prava što obrisati, editirati, vidjeti.
