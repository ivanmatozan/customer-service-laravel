<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Home
Route::get('/', [
    'middleware' => 'auth',
    'uses' => 'RoleController@home',
]);

Route::get('question', function () {
    return view('pages.question');
});

// Store question
Route::post('question/store', 'QuestionController@store');
// Get question
Route::get('question/{question}', 'QuestionController@display');
// Delete question
Route::get('question/{question}/delete', 'QuestionController@delete');
// Close question
Route::get('question/{question}/close', 'QuestionController@close');
// Open question
Route::get('question/{question}/open',  'QuestionController@open');
// Edit question
Route::get('question/{question}/edit', 'QuestionController@edit');
// Update question
Route::patch('question/{question}/update', 'QuestionController@update');

// Store response
Route::post('response/{question}/store', 'ResponseController@store');
// Delete response
Route::get('response/{response}/delete', 'ResponseController@delete');
// Edit response
Route::get('response/{response}/edit', 'ResponseController@edit');
// Update response
Route::patch('response/{response}/update', 'ResponseController@update');


Route::auth();
