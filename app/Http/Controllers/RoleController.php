<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use App\Http\Requests;

class RoleController extends Controller
{
    public function home(Request $request) {

        // Authenticated user
        $user = $request->user();

        $questions = Question::where('user_id', '=', $user->id)->orderBy('active', 'desc')->orderBy('created_at', 'desc')->get();

        // If user is admin return AdminController else return RegularController
        if ($user->isAdmin()) {
            $allQuestions = Question::orderBy('active', 'desc')->orderBy('created_at', 'asc')->get();
            return view('pages.admin', [
                'questions' => $allQuestions,
            ]);
        }
        
        return view('pages.regular', [
            'questions' => $questions,
        ]);
    }
}
